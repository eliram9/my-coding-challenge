// **Find Three Largest Numbers (1 point):** 

/*Write a function that takes in an array of integers and returns a sorted array of the three largest integers in the input array.
The function should return duplicate integers if necessary; for example, it should return `[10, 10, 12]` for an input array of `[10, 5, 9, 10, 12]`.*/

const nums = [141, 1, 17, -7, -17, -417, 99, 75, 541, 8,7, 7];

displayTopThree = (myArr) => {
  let sortedArr = myArr.sort(function(a,b) { 
      return b-a; 
    }).slice(0,3);
  console.log('1. The three top numbers are: ',sortedArr);
}

const myArr = nums;
displayTopThree(myArr);

//--------------------------------------------------------------------------------------------------------------------------//

//**Palindrome Check (1 point):** 

/*Write a function that takes in a non-empty string, and that returns a boolean representing whether the string is a palindrome.
A palindrome is defined as a string that's written the same forward and backward. *Note*: a single-character string is not a palindrome. */

paliCheck = (myStr) => {
    if(myStr !== null && myStr !== '' && myStr.length >= 2) {
        let strRev = myStr.split("").reverse().join("");
        if (myStr === strRev) {
            console.log('2.', true);
        }
            else console.log('2.' ,false);
    }
    else console.log('N/A')
}
  
paliCheck('abababa');

//--------------------------------------------------------------------------------------------------------------------------//

//**Caesar Cipher Encryptor (1 point):** 

/*Given a non-empty string of lowercase letters and a non-negative integer representing a key, write a function that returns a new string obtained by shifting every letter in the input string by `k` positions in the alphabet, where k is the key.
Note that letters should "wrap" around the alphabet; in other words, the letter `z` shifted by one returns the letter `a`.*/

hideInput = (inputStr, key) => {
    const letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", 
                     "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
   
    let newInputStr = inputStr.split("");
    let indexVal = 0;
    let results = [];
        for (let index in newInputStr) {
            if (newInputStr[index] != "z") {
                indexVal = letters.indexOf(newInputStr[index]) + key;
            }

            newInputStr[index] = letters[indexVal];
        
            let crypticInputStr = newInputStr[index];
            results.push(crypticInputStr)
        }
    console.log('3. Hidden input: ', results.join(""));    
} 

hideInput("barbarian", 2);

//--------------------------------------------------------------------------------------------------------------------------//

//**Bubble Sort (1 points):** 

/*Write a function that takes in an array of integers and returns a sorted version of that array. Use the Bubble Sort algorithm to sort the array.*/

//first time I'm doing this bubble sort algorithm.

const bubbleSortArr = [8, 5, 2, 9, 5, 6, 3];

bubbleSort = (bubbleSortArr) => {
    let i, j, limit;
    // the limit is the index of loops based on the array length.
    limit = bubbleSortArr.length;
    while(limit--) {
        for (i = 0, j = 1; i < limit; ++i, ++j) 
            // if the variavle on the left is greater than the one on the right then swap their positions, else move forward.
            if (bubbleSortArr[i] > bubbleSortArr[j]) {
                temp = bubbleSortArr[i];
                bubbleSortArr[i] = bubbleSortArr[j];
                bubbleSortArr[j] = temp;
            }
    }
    console.log('4. ', bubbleSortArr);
}

bubbleSort(bubbleSortArr);

//--------------------------------------------------------------------------------------------------------------------------//

//**Min Number of Coins for Change (3 points):** 

/*Given an array of positive integers representing coin denominations and a single non-negative integer `n` representing a target amount of money, write a function that returns the smallest number of coins needed to make change for that target amount using the given coin denominations. 
If it's impossible to make change for the target amount, return `-1`.
Note that an unlimited amount of coins is at your disposal.*/



